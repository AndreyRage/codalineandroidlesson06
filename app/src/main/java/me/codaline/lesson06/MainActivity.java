package me.codaline.lesson06;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = MainActivity.class.getSimpleName();

    private static final String URL_IP = "http://ip2country.sourceforge.net/ip2c.php";

    private TextView mTextView;
    private View mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextView = (TextView) findViewById(R.id.text_view);
        mProgressBar = findViewById(R.id.progress_bar);
        findViewById(R.id.btn_http_url_connection).setOnClickListener(this);
        findViewById(R.id.btn_ok_http).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_http_url_connection:
                new AsyncTask<String, Void, Boolean>() {
                    String load;

                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
                        mProgressBar.setVisibility(View.VISIBLE);
                        mTextView.setText("");
                    }

                    @Override
                    protected Boolean doInBackground(String... params) {
                        if (isConnected()) {
                            try {
                                URL url = new URL(params[0]);
                                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                                conn.setReadTimeout(10000 /* milliseconds */);
                                conn.setConnectTimeout(15000 /* milliseconds */);
                                conn.setRequestMethod("GET");
                                conn.setDoInput(true);
                                // Starts the query
                                conn.connect();
                                int response = conn.getResponseCode();
                                Log.d(TAG, "The response is: " + response);
                                InputStream is = conn.getInputStream();

                                StringBuilder sb = new StringBuilder();
                                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                                String read;

                                while ((read = br.readLine()) != null) {
                                    sb.append(read);
                                }

                                load = sb.toString();

                                br.close();
                                is.close();

                                return true;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        return false;
                    }

                    @Override
                    protected void onPostExecute(Boolean aBoolean) {
                        super.onPostExecute(aBoolean);
                        if (aBoolean) {
                            mTextView.setText(load);
                        } else {
                            mTextView.setText("error");
                        }
                        mProgressBar.setVisibility(View.GONE);
                    }
                }.execute(URL_IP);
                break;
            case R.id.btn_ok_http:
                MyLoadTask task = new MyLoadTask(mTextView, mProgressBar);
                task.execute(URL_IP);
                break;
        }
    }

    private boolean isConnected() {
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}
