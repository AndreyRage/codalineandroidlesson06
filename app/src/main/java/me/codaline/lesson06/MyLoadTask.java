package me.codaline.lesson06;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by rage on 13.03.16.
 */
public class MyLoadTask extends AsyncTask<String, Void, Boolean> {
    private static final String TAG = MyLoadTask.class.getSimpleName();

    private OkHttpClient mOkHttpClient = new OkHttpClient();

    private String mLoad;

    private TextView mTextView;
    private View mProgressBar;

    public MyLoadTask(TextView textView, View progressBar) {
        mTextView = textView;
        mProgressBar = progressBar;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mProgressBar.setVisibility(View.VISIBLE);
        mTextView.setText("");
    }

    @Override
    protected Boolean doInBackground(String... params) {

        //imitate hard work
        /*try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/


        //request
        Request request = new Request.Builder()
                .url(params[0])
                .build();
        try {
            Response response = mOkHttpClient.newCall(request).execute();
            int code = response.code();
            Log.d(TAG, "The response is: " + code);

            mLoad = response.body().string();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        if (aBoolean) {
            mTextView.setText(mLoad);
        } else {
            mTextView.setText("error");
        }
        mProgressBar.setVisibility(View.GONE);
    }
}
